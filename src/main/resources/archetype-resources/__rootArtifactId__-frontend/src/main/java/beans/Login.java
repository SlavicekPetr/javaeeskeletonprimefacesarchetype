#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.beans;

import ${package}.domain.User;
import ${package}.interfaces.repository.IUserRepository;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author slavicekp
 */
@Named(value = "login")
@Dependent
public class Login {

	@EJB
	private IUserRepository userRepository;

	private User user = null;

	/**
	 * Creates a new instance of Login
	 */
	public Login() {
	}

	public User getLoggedUser() {
		String remoteUser = FacesContext.getCurrentInstance().getExternalContext().getRemoteUser();
		if (remoteUser == null) {
			user = null;
		} else if (user == null) {
			user = userRepository.findByLogin(remoteUser);
		}
		return user;
	}
	
	public String getLoggedUserName() {
		if (user != null) {
			return user.getFirstName() + " " + user.getLastName();
		}
		return "";
	}

	public String logout() {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		try {
			request.logout();
		} catch (ServletException e) {
			context.addMessage(null, new FacesMessage("Logout failed."));
		}
		return "/index?faces-redirect=true";
	}

}
