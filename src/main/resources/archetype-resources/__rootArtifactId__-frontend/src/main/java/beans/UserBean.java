#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.beans;

import ${package}.interfaces.repository.IUserRepository;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import ${package}.domain.User;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author slavicekp
 */
@ManagedBean(name = "userBean")
@SessionScoped
public class UserBean implements Serializable {
	
	@EJB
	private IUserRepository userRepository;
	
	private List<User> users;
	
	private User user;

	/**
	 * Creates a new instance of User
	 */
	public UserBean() {		
	}
	
	@PostConstruct
	public void intit() {
		users = userRepository.findAll();
	}
	
	public List<User> getAllUsers() {
		return users;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public void persistUser() {
		userRepository.persist(user);
		FacesMessage message = new FacesMessage();
        message.setSeverity(FacesMessage.SEVERITY_INFO);
        message.setSummary("User saved!");
        FacesContext.getCurrentInstance().addMessage(null, message);
	}
	
	public void createUser() {
		user = new User();
	}
	
	public void editUser(User user) {
		this.user = userRepository.findById(user.getId());
	}
	
}
