#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId};

import java.net.URL;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Produces;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import ${artifactId}.integration.cdi.CDILiquibaseConfig;
import ${artifactId}.integration.cdi.annotations.LiquibaseType;
import ${artifactId}.resource.ClassLoaderResourceAccessor;
import ${artifactId}.resource.ResourceAccessor;

/**
 *
 * @author slavicekp
 */
@Dependent
public class LiquibaseIntegration {
	
	private DataSource myDataSource;

	@Produces
	@LiquibaseType
	public CDILiquibaseConfig createConfig() {
		CDILiquibaseConfig config = new CDILiquibaseConfig();
		config.setChangeLog("db_migration/changelog.xml");
		return config;
	}

	@Produces
	@LiquibaseType
	public DataSource createDataSource() throws SQLException {
		try {
			myDataSource = InitialContext.doLookup("${jdbc-resource-name}");
		} catch (NamingException ex) {
			Logger.getLogger(LiquibaseIntegration.class.getName()).log(Level.SEVERE, null, ex);
		}
		return myDataSource;
	}

	@Produces
	@LiquibaseType
	public ResourceAccessor create() {
		return new ClassLoaderResourceAccessor(getClass().getClassLoader());
	}
}
