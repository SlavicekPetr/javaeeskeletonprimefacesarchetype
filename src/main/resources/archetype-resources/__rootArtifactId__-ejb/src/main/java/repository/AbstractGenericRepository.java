#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.repository;

import ${package}.interfaces.repository.IAbstractRepository;
import java.lang.reflect.ParameterizedType;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;
import javax.persistence.PersistenceContext;


public abstract class AbstractGenericRepository<E> implements IAbstractRepository<E> {
    
    private final Class<E> clazz;
	
	@PersistenceContext
	private EntityManager em;
	

    protected AbstractGenericRepository() {
		this.clazz = (Class<E>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }
	
	@Override
	public E create()throws IllegalAccessException, InstantiationException {
		return this.clazz.newInstance();
	}

	@Override
    public List<E> findAll() {
		return findAllWithLimit(null);
    }
	
	@Override
    public List<E> findAllWithLimit(Integer limit) {
        TypedQuery<E> query = this.getEntityManager().createQuery("FROM " + this.clazz.getSimpleName(), this.clazz);
		if (limit != null) {
			query.setMaxResults(limit);
		}
		List<E> entities = query.getResultList();
		return entities;
    }

	@Override
    public E findById(long id) {
        E entity = this.getEntityManager().find(this.clazz, id);
        return entity;
    }

	@Override
    public void persist(E entity) {
		E merged = this.getEntityManager().merge(entity);
        this.getEntityManager().persist(merged);
    }

    protected Class<E> getEntityClass(){
		return clazz;
	}

    protected EntityManager getEntityManager() {
		return em;
	}

}
