#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service;

import ${package}.interfaces.ISecurityService;
import javax.annotation.Resource;
import javax.annotation.security.DeclareRoles;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author slavicekp
 */
@Stateless
@DeclareRoles(SecurityService.AUTH_USER_ROLE)
public class SecurityService implements ISecurityService {
	
	public static final String AUTH_USER_ROLE = "AUTHENTICATED_USER";

	@PersistenceContext
	EntityManager em;
	
	@Resource
	private SessionContext ctx;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SecurityService.class);
	
	@Override
	public boolean isUserLogged() {
		ctx.getCallerPrincipal();
		return ctx.isCallerInRole(AUTH_USER_ROLE);
	}
	
	@Override
	public void logout() {
		
	}
}
