#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.repository;

import ${package}.domain.User;
import ${package}.interfaces.repository.IUserRepository;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author slavicekp
 */
@Singleton
@Lock(LockType.READ)
public class UserRepository extends AbstractGenericRepository<User> implements IUserRepository {
	
	@Override
	public User findByLogin(String login) {
		Query query = this.getEntityManager().createNamedQuery(User.FIND_BY_LOGIN, User.class);
		query.setParameter("login", login);
		return (User) query.getSingleResult();
	}	

}
