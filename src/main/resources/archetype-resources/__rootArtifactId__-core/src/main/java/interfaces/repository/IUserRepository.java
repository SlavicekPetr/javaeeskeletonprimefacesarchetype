#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.interfaces.repository;

import ${package}.domain.User;
import javax.ejb.Local;

/**
 *
 * @author slavicekp
 */
@Local
public interface IUserRepository extends IAbstractRepository<User>{
	
	User findByLogin(String login);

}
