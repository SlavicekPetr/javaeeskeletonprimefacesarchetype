#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.interfaces.repository;

import java.util.List;

/**
 *
 * @author slavicekp
 */
public interface IAbstractRepository<E> {

	E create() throws IllegalAccessException, InstantiationException;

	List<E> findAll();

	List<E> findAllWithLimit(Integer limit);

	E findById(long id);

	void persist(E entity);

}
