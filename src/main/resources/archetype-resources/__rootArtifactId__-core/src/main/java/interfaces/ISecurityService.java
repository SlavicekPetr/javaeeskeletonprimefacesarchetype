#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.interfaces;

import javax.ejb.Local;

/**
 *
 * @author slavicekp
 */
@Local
public interface ISecurityService {
	
	boolean isUserLogged();
	
	void logout();

}
